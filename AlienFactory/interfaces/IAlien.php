<?php
interface IAlien {
  public function getNombre();
  public function getEdad();
  public function getEspecie();
  public function getPlaneta();
  public function getMoral();

  public function setNombre($nombre);
  public function setEdad($edad);
  public function setEspecie($especie);
  public function setPlaneta($planeta);
  public function setMoral($moral);

  public function whoIAm();

}
