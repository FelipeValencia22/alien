<?php
interface IPlanet {
    public function getNombre();
    public function getEstado();

    public function setNombre($nombre);
    public function setEstado($estado);
}
