<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Planet
 *
 * @author pabhoz
 */
class Planet implements IPlanet {
  private $nombre;
  private $estado;

  function __construct($nombre, $estado) {
      $this->nombre = $nombre;
      $this->estado = $estado;
  }

  function getNombre() {
       return $this->nombre;
   }

   function getEstado() {
       return $this->estado;
   }

   function setNombre($nombre) {
       $this->nombre = $nombre;
   }

   function setEstado($estado) {
       $this->estado = $estado;
   }

   function status(){
     $this->getEstado();
   }

}
