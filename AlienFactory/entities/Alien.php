<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of Alien
 *
 * @author pabhoz
 */
class Alien implements IAlien{

    protected $nombre;
    protected $edad;
    protected $especie;
    protected $planeta;
    private $moral;

    protected static $comunicacion="Telepaticamente";

    function __construct($nombre, $edad, $especie, $planeta) {
       $this->nombre = $nombre;
       $this->edad = $edad;
       $this->especie = $especie;
       $this->planeta = $planeta;
   }

   function getNombre() {
     return $this->nombre;
   }

   function getEdad() {
     return $this->edad;
   }

   function getEspecie() {
     return $this->especie;
   }

   function getPlaneta() {
     return $this->planeta;
   }

   function getMoral() {
     return $this->moral;
   }

   function setNombre($nombre) {
     $this->nombre = $nombre;
   }

   function setEdad($edad) {
     $this->edad = $edad;
   }

   function setEspecie($especie) {
     $this->especie = $especie;
   }

   function setPlaneta($planeta) {
     $this->planeta = $planeta;
   }

   function setMoral($moral) {
     $this->moral = $moral;
   }

   static function getComunicacion() {
      return self::$comunicacion;
  }

  static function setComunicacion($comunicacion) {
      self::$comunicacion = $comunicacion;
  }

   public function interact(){
     print_r($this->getComunicacion()."Hola terricola, rindanse ante la invasión de ".$this->getPlaneta());
   }

   public function whoIAm(){
     print_r("Mi nombre es ".$this->getNombre().", vengo del plantea ".$this->getPlaneta().", soy un ".$this->getEspecie()." y soy ".$this->getMoral());
   }

}
