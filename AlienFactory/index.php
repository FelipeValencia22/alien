<?php

spl_autoload_register(function($class) {
    if (file_exists("entities/" . $class . ".php")) {
        include "entities/" . $class . ".php";
        return false;
    }
    if (file_exists("factories/" . $class . ".php")) {
        include "factories/" . $class . ".php";
        return false;
    }
    if (file_exists("interfaces/" . $class . ".php")) {
        include "interfaces/" . $class . ".php";
        return false;
    }
    if (file_exists("bridges/" . $class . ".php")) {
        include "bridges/" . $class . ".php";
        return false;
    }
});

function say($what){
    print "</br>".$what."</br>";
}

$alien = AlienFactory::getAlien("Ancient",9999,"Primerus Serus","Sun");
$jalien = AlienFactory::getJupiterAlien("Jane",212,"Jupiterus Corvus");
$malien = AlienFactory::getMarsAlien("Marcus",312,"Martianus Mexumos");
$moalien = AlienFactory::getMoonAlien("Mulen",123,"Lunaticus");
$palien = AlienFactory::getPlutoAlien("Pluterus",123,"Plutiretus Minus");
$salien = AlienFactory::getSaturnAlien("Sandros",12,"Saturicus Olus");
$valien = AlienFactory::getVenusAlien("Valerixolus",39,"Seductorus Facilus");

say($alien->whoIAm());
say($jalien->whoIAm());
say($malien->whoIAm());
say($moalien->whoIAm());
say($palien->whoIAm());
say($salien->whoIAm());
say($valien->whoIAm());

say($jalien->pedirRefuerzos());

$planet = new Planet("tierra","a salvo");
say($jalien->destruirPlaneta($planet));
say($planet->status());

say($alien->interact());
say($jalien->interact());
say($malien->interact());
say($moalien->interact());
say($palien->interact());
say($salien->interact());
say($valien->interact());

say($valien->llamarACasa());
say($valien->salvarPlaneta($planet));
say($planet->status());
