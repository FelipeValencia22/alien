<?php

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 * Description of GoodAlien
 *
 * @author pabhoz
 */
class GoodAlien extends Alien implements IGoodAlien{
  protected static $moral;

  public function __construct() {
      self::$moral = "Bueno";
  }

  public function llamarACasa(){
    print_r("Llamando a Caaaaasaaaaa");
  }

  public function salvarPlaneta(Planet $planet){
    $planet->setEstado("Plantea ".$planet->getNombre().": a salvo");
    print_r($planet->getEstado());
  }

}
