<?php

require "../entities/Alien.php";
require "../interfaces/IAlien.php";
class AlienTest extends \Codeception\Test\Unit
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    // tests
    public function testMe(){

      $alien= new Alien("Felipe",22,"TWD","Tierra");
      \Codeception\Util\Debug::debug($alien);
      $this->assertTrue($alien->getNombre() == "Felipe");
    }
}
